FROM --platform=linux/amd64 python:3.11 

ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]

RUN apt-get update && apt-get install poppler-utils -y && apt-get clean

# Libreoffice
RUN apt-get install software-properties-common python3-launchpadlib -y
RUN add-apt-repository ppa:libreoffice/ppa
RUN apt-get update && apt-get upgrade
RUN apt-get install libreoffice -y
RUN apt-get clean

######## adds custom Marianne font ########
COPY ressources/marianne.tar.gz /tmp/marianne.tar.gz
RUN tar -xvf /tmp/marianne.tar.gz -C /usr/local/share/fonts

WORKDIR /app

EXPOSE 3000

RUN pip install poetry

COPY poetry.lock pyproject.toml /app/

RUN poetry config virtualenvs.create false \
  && poetry install --only main --no-interaction --no-ansi --no-root

COPY . /app

ENV WORKER_POOL="4"

CMD ["sh", "-c", "gunicorn \"sign_api.app:create_app()\" -w $WORKER_POOL -b 0.0.0.0:3000 --log-level=info --log-config=logging_config.ini --access-logformat '%(r)s %(s)s %(a)s'"]
