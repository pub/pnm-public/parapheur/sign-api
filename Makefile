PORT ?= "3000"
run:
	poetry run flask --app sign_api.app --debug run --host=0.0.0.0 --port $(PORT)


WORKER_POOL ?= "4"
run_prod:
	poetry run gunicorn -w $(WORKER_POOL) "sign_api.app:app" -b 0.0.0.0:$(PORT) --log-config=logging_config.ini --access-logformat '%(m)s %(U)s %(s)s'

test:
	poetry run pytest --cov=sign_api --cov-report=term-missing --cov-report=xml

test_watch:
	poetry run ptw -- -- --cov=sign_api --cov-report=term-missing --cov-report=xml