from logging.config import fileConfig
import logging


fileConfig("logging_config.ini")


def get_logger():
    # To use the gunicorn logger when in production, the simplest way is to always name the logger "gunicorn.error"
    return logging.getLogger("gunicorn.error")
