from flask import Flask
from sign_api.app import create_app


def describe_the_create_app_function():
    def should_create_the_app(base_env):
        # when
        app = create_app()

        # then
        assert app is not None
        assert isinstance(app, Flask)

    def should_register_the_healthcheck_endpoint(base_env):
        # given
        app = create_app()

        # when
        response = app.test_client().get("/")

        # then
        assert response.status_code == 200
        assert response.data == b"Hello World!"
