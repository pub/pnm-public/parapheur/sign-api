from flask import Flask, request
import pytest
from sign_api.container import Container

from sign_api.handlers import handle_exception, parse_body
from werkzeug import exceptions


def describe_the_parse_body_handler():
    def should_parse_the_body():
        # given
        app = Flask(__name__)
        app.before_request(parse_body)

        @app.post("/test")
        def test_route():
            return request.body

        # when
        response = app.test_client().post("/test", json={"foo": "bar"})

        # then
        assert response.status_code == 200
        assert response.json == {"foo": "bar"}

    def should_set_empty_body_when_none_is_provided():
        # given
        app = Flask(__name__)
        app.before_request(parse_body)

        @app.post("/test")
        def test_route():
            return request.body

        # when
        response = app.test_client().post("/test")

        # then
        assert response.status_code == 200
        assert response.json == {}

    def should_set_empty_body_when_invalid_is_provided():
        # given
        app = Flask(__name__)
        app.before_request(parse_body)

        @app.post("/test")
        def test_route():
            return request.body

        # when
        response = app.test_client().post("/test", data="toto")

        # then
        assert response.status_code == 200
        assert response.json == {}


def describe_the_handle_exception_handler():
    @pytest.fixture
    def container(base_env):
        container = Container()
        container.wire(modules=[__name__, "sign_api.handlers"])
        yield container
        container.unwire()

    def should_return_500_for_unknown_errors(container: Container):
        # given
        app = Flask(__name__)
        app.before_request(parse_body)
        app.errorhandler(Exception)(handle_exception())

        @app.get("/test")
        def test_route():
            raise Exception("toto")

        # when
        response = app.test_client().get("/test")

        # then
        assert response.status_code == 500
        assert response.json == {
            "code": 500,
            "name": "Internal Server Error",
            "description": "toto",
        }

    def should_return_provided_status_code(container: Container):
        # given
        app = Flask(__name__)
        app.before_request(parse_body)
        app.errorhandler(Exception)(handle_exception())

        @app.get("/test")
        def test_route():
            raise exceptions.BadRequest("toto")

        # when
        response = app.test_client().get("/test")

        # then
        assert response.status_code == 400
        assert response.json == {
            "code": 400,
            "name": "Bad Request",
            "description": "toto",
        }
