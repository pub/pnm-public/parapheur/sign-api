import os
import pytest
from sign_api.settings import Settings


from sign_api.conftest import MockEnv


def describe_the_settings_class():
    def describe_init():
        def should_not_raise_when_config_is_valid(mock_env: MockEnv):
            # given
            mock_env(MINIO_ACCESS_KEY="tota", MINIO_SECRET_KEY="toto")

            # when
            settings = Settings()

            # then
            assert settings is not None
            assert settings.MINIO["ACCESS_KEY"] == "tota"

        def should_raise_when_config_is_invalid(monkeypatch: pytest.MonkeyPatch):
            with pytest.raises(Exception):
                Settings()

    def describe_workdir():
        @pytest.fixture(autouse=True)
        def before_each(base_env):
            pass

        def should_return_default_workdir():
            settings = Settings()
            assert settings.WORKDIR == "/tmp/sign_api"

        def should_return_workdir(mock_env: MockEnv):
            # given
            expected = "/tmp/sign_api2"
            mock_env(WORKDIR=expected)

            # when
            settings = Settings()

            # then
            assert settings.WORKDIR == expected

        def should_create_folder(mock_env: MockEnv):
            # given
            expected = "/tmp/sign_api3"
            mock_env(WORKDIR=expected)
            assert not os.path.exists(expected)

            # when
            settings = Settings()

            # then
            assert settings.WORKDIR == expected
            assert os.path.exists(settings.WORKDIR)

        def should_create_deep_folder(mock_env: MockEnv):
            # given
            expected = "/tmp/toto/tata/sign_api3"
            mock_env(WORKDIR=expected)
            assert not os.path.exists(expected)

            # when
            settings = Settings()

            # then
            assert settings.WORKDIR == expected
            assert os.path.exists(settings.WORKDIR)
            path = settings.WORKDIR
            while (parent := os.path.dirname(path)) and path != "/":
                assert os.path.exists(parent)
                path = parent
