import base64
import os
import hashlib


def get_task_dir(workdir: str, task_id: str):
    task_dir = os.path.join(workdir, task_id)
    os.makedirs(task_dir, exist_ok=True)
    return task_dir


def without_keys(d: dict, keys: list):
    return {k: d[k] for k in d.keys() - keys}


def to_base64(string: str):
    return base64.b64encode(string.encode()).decode()


def to_md5(string: str):
    return hashlib.md5(string.encode()).hexdigest()
