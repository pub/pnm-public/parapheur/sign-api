from logging import Logger
from pathlib import Path
from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler
from dependency_injector.wiring import inject, Provide

from sign_api.settings import Settings


@inject
def clean_work_dir(
    logger: Logger = Provide["logger"], settings: Settings = Provide["settings"]
):
    logger.info("Cleaning work dir")
    workdir = Path(settings.WORKDIR)
    for file in Path(settings.WORKDIR).rglob("*"):
        logger.info(f"Cleaning {file}")
        try:
            if (
                file.is_file()
                and file.stat().st_mtime < datetime.now().timestamp() - 3600
            ):
                file.unlink()
        except OSError as e:  # pragma: nocover
            logger.error(f"Error: {e.filename} - {e.strerror}.")  # pragma: nocover

    for folder in workdir.rglob("*"):
        logger.info(f"Cleaning {folder}")
        try:
            if folder.is_dir() and list(folder.glob("*")) == []:
                folder.rmdir()
        except OSError as e:  # pragma: nocover
            logger.error(f"Error: {e.filename} - {e.strerror}.")  # pragma: nocover


def create_scheduler():
    scheduler = BackgroundScheduler(timezone="UTC")
    scheduler.add_job(
        clean_work_dir,
        "cron",
        minute="0",
        hour="0",
        id="clean_work_dir",
        misfire_grace_time=None,
    )
    scheduler.start()
    return scheduler
