from pathlib import Path
import pytest
from sign_api.services.minio_service import MinioService
from sign_api.container import Container


class MockResponse:
    def __init__(self, data):
        self.data = data

    def read(self):
        return self.data


def describe_the_minio_service_class():
    @pytest.fixture
    def minio_service(base_env):
        container = Container()
        container.wire(modules=[__name__, "sign_api.services.minio_service"])
        service = MinioService()
        service.bucket = "test_bucket"
        yield service
        container.unwire()

    def describe_the_init():
        def should_initialize_the_client_with_proxy(
            base_env, monkeypatch: pytest.MonkeyPatch
        ):
            monkeypatch.setenv("MINIO_PROXY", "http://toto.com:3128")
            container = Container()
            container.wire(modules=[__name__, "sign_api.services.minio_service"])

            # when
            minio_service = MinioService()

            # then
            assert minio_service.client._http.proxy.url == "http://toto.com:3128"
            container.unwire()

        def should_initialize_the_client_without_proxy(minio_service: MinioService):
            assert minio_service.settings.MINIO["PROXY"] is None
            assert minio_service.client._http.proxy is None

    def describe_the_get_file_method():
        def should_return_the_filepath(
            minio_service: MinioService, monkeypatch: pytest.MonkeyPatch
        ):
            # given
            def mock_get_object(bucket: str, key: str):
                assert bucket == "test_bucket"
                assert key == "key"
                return MockResponse(b"content_file")

            monkeypatch.setattr(minio_service.client, "get_object", mock_get_object)

            # when
            result = minio_service.get_file("key", "/destination")

            # then
            assert result == Path("/destination")
            assert result.exists()
            assert result.read_bytes() == b"content_file"

    def describe_the_put_file_method():
        def should_return_the_filepath(
            minio_service: MinioService, monkeypatch: pytest.MonkeyPatch
        ):
            # given
            def mock_fput_object(
                bucket: str, key: str, source_path: str, content_type: str
            ):
                assert bucket == "test_bucket"
                assert key == "key"
                assert source_path == "/source.pdf"
                assert content_type == "application/pdf"
                return MockResponse(b"content_file")

            monkeypatch.setattr(minio_service.client, "fput_object", mock_fput_object)

            # when
            result = minio_service.put_file("/source.pdf", "key")

            # then
            assert result == "key"

    def describe_the_get_signed_url_method():
        def should_return_the_signed_url(
            minio_service: MinioService, monkeypatch: pytest.MonkeyPatch
        ):
            # given
            def mock_presigned_get_object(bucket: str, key: str):
                assert bucket == "test_bucket"
                assert key == "key"
                return "signed_url"

            monkeypatch.setattr(
                minio_service.client, "presigned_get_object", mock_presigned_get_object
            )

            # when
            result = minio_service.get_signed_url("key")

            # then
            assert result == "signed_url"
