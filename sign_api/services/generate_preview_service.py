from pathlib import Path
import pdf2image
from sign_api.settings import Settings
from dependency_injector.wiring import Provide
from sign_api.utils import to_md5


class GeneratePreviewService:
    settings: Settings = Provide["settings"]

    def get_document_key(self, document: str) -> Path:
        document_key = (
            Path(self.settings.WORKDIR) / "generate_preview" / f"{to_md5(document)}.pdf"
        )

        if not document_key.exists():
            document_key.parent.mkdir(parents=True, exist_ok=True)

        return document_key

    def generate_single_page(
        self,
        document: Path,
        task_dir: Path,
        page: int,
    ) -> list[Path]:
        images = pdf2image.convert_from_path(
            document,
            dpi=72 * self.settings.DPI_FACTOR,
            first_page=page + 1,  # 1 based indexing
            last_page=page + 1,
        )

        path = task_dir / f"{page}.png"
        images[0].save(path, "PNG")

        return [path]

    def generate_whole_document(
        self,
        document: Path,
        task_dir: Path,
    ) -> list[Path]:
        images = pdf2image.convert_from_path(
            document, dpi=72 * self.settings.DPI_FACTOR
        )

        result = []

        for i, image in enumerate(images):
            path = task_dir / f"{i}.png"
            image.save(path, "PNG")
            result.append(path)

        return result
