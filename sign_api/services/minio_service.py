from logging import Logger
from mimetypes import guess_type
from pathlib import Path
from minio import Minio
from dependency_injector.wiring import Provide
import urllib3

from sign_api.settings import Settings


class MinioService:
    settings: Settings = Provide["settings"]
    logger: Logger = Provide["logger"]

    def __init__(self):
        proxy_params = (
            {
                "http_client": urllib3.ProxyManager(
                    self.settings.MINIO["PROXY"],
                )
            }
            if self.settings.MINIO.get("PROXY", None)
            else {}
        )

        self.client = Minio(
            self.settings.MINIO["ENDPOINT"],
            access_key=self.settings.MINIO["ACCESS_KEY"],
            secret_key=self.settings.MINIO["SECRET_KEY"],
            secure=self.settings.MINIO["SECURE"],
            region=self.settings.MINIO["REGION"],
            **proxy_params,
        )

        self.bucket = self.settings.MINIO["BUCKET"]

    def get_file(self, source_key: str, target_path: str | Path) -> Path:
        self.logger.info(f"Retrieving {source_key} from Minio...")
        response = self.client.get_object(self.bucket, source_key)
        self.logger.info(f"Retrieved {source_key} from Minio")

        with open(target_path, "wb") as file:
            file.write(response.data)

        return Path(target_path)

    def put_file(self, source_path: str, key_destination: str) -> str:
        self.logger.info(f"Uploading {key_destination} to Minio...")
        self.client.fput_object(
            self.bucket,
            key_destination,
            source_path,
            content_type=guess_type(source_path)[0],
        )
        self.logger.info(f"Uploaded {key_destination} to Minio")

        return key_destination

    def get_signed_url(self, key: str) -> str:
        return self.client.presigned_get_object(self.bucket, key)
