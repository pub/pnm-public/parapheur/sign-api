from logging import Logger
from pathlib import Path
from reportlab.pdfgen import canvas
from pypdf import PdfReader, PdfWriter
from pypdf.constants import AnnotationDictionaryAttributes

from sign_api.domain.sign_coordinates import SignCoordinates
from dependency_injector.wiring import Provide

from sign_api.settings import Settings


class SignService:
    logger: Logger = Provide["logger"]
    settings: Settings = Provide["settings"]

    def sign(
        self,
        workdir: Path,
        pdf_path: Path,
        signatures: list[SignCoordinates],
    ):
        pdf_signature_paths: list[str] = []
        for signature in signatures:
            pagesize = self.get_pagesize(pdf_path, signature.page)
            pdf_signature_paths.append(
                self.convert_signature_to_pdf(workdir, pagesize, signature)
            )

        writer = PdfWriter(clone_from=pdf_path)

        for coordinates, pdf_signature_path in zip(signatures, pdf_signature_paths):
            self.logger.info(
                f"Signing {pdf_path} at page {coordinates.page} with signature {pdf_signature_path}"
            )
            signature = PdfReader(open(pdf_signature_path, "rb"))
            fields = []
            if writer.pages[coordinates.page].annotations:
                for annot in writer.pages[coordinates.page].annotations:
                    annot = annot.get_object()
                    if annot[AnnotationDictionaryAttributes.Subtype] == "/Widget":
                        fields.append(annot)
                writer.pages[coordinates.page].annotations.clear()
                writer.pages[coordinates.page].annotations.extend(fields)
            writer.pages[coordinates.page].transfer_rotation_to_content()
            writer.pages[coordinates.page].merge_page(signature.pages[0])
            final_path = workdir / f"{pdf_path.stem}.signed.pdf"
            self.logger.info(
                f"Signed {pdf_path} at page {coordinates.page} with signature {pdf_signature_path} to {final_path}"
            )
        with open(final_path, "wb") as output_stream:
            writer.write(output_stream)
        return final_path

    def convert_signature_to_pdf(
        self,
        workdir: Path,
        pagesize: list[int],
        coordinates: SignCoordinates,
    ) -> Path:
        signature_path = coordinates.path
        self.logger.info(f"Converting signature {signature_path} to PDF")
        pdf_signature_path = workdir / coordinates.get_pdf_filename()
        signature_canvas = canvas.Canvas(str(pdf_signature_path), pagesize=pagesize)
        signature_canvas.drawImage(
            str(signature_path),
            x=coordinates.x / self.settings.DPI_FACTOR,
            y=coordinates.y / self.settings.DPI_FACTOR
            - coordinates.height / self.settings.DPI_FACTOR,  # nw anchor doesn't work
            height=coordinates.height / self.settings.DPI_FACTOR,
            width=coordinates.width / self.settings.DPI_FACTOR,
            preserveAspectRatio=True,
            # showBoundary=True,
            anchor="sw",
            mask="auto",
        )
        signature_canvas.save()
        self.logger.info(
            f"Converted signature {signature_path} to PDF at {pdf_signature_path}"
        )
        writer = PdfWriter(clone_from=pdf_signature_path)

        writer.write(pdf_signature_path)
        return pdf_signature_path

    def get_pagesize(self, pdf_path: Path, page: int) -> list[int]:
        self.logger.info(f"Getting pagesize for page {page} of {pdf_path}")
        original_file_reader = PdfReader(pdf_path)
        _, _, *pagesize = original_file_reader.pages[page].mediabox
        if original_file_reader.pages[page].rotation > 0:
            pagesize.reverse()
        self.logger.info(f"Got pagesize {pagesize} for page {page} of {pdf_path}")
        return pagesize
