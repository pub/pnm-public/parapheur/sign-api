from pathlib import Path
import pdf2image
import pytest
from sign_api.services.generate_preview_service import GeneratePreviewService
from sign_api.settings import Settings
from sign_api.utils import get_task_dir, to_md5
from PIL import Image


def describe_the_generate_preview_service_class():
    @pytest.fixture
    def generate_preview_service(base_env: None):
        generate_preview_service = GeneratePreviewService()
        generate_preview_service.settings = Settings()
        return generate_preview_service

    def describe_the_get_document_key_method():
        @pytest.mark.parametrize("document_name", ["toto", "/tata/toto.pdf"])
        def should_returns_a_path_object_with_the_correct_path(
            generate_preview_service: GeneratePreviewService,
            document_name: str,
        ):
            # given
            document_name = "test"
            hash = to_md5(document_name)

            # when
            document_key = generate_preview_service.get_document_key(document_name)

            # then
            assert (
                document_key
                == Path(generate_preview_service.settings.WORKDIR)
                / "generate_preview"
                / f"{hash}.pdf"
            )

    def describe_the_generate_preview_single_page_method():
        def should_call_pdf2image(
            generate_preview_service: GeneratePreviewService,
            monkeypatch: pytest.MonkeyPatch,
        ):
            # given
            document_key = generate_preview_service.get_document_key("test")
            page = 1
            task_dir = Path(
                get_task_dir(generate_preview_service.settings.WORKDIR, "test")
            )

            def mock_pdf2image(
                document: str, dpi: int, first_page: int, last_page: int
            ):
                assert str(document) == str(document_key)
                assert dpi == 72 * generate_preview_service.settings.DPI_FACTOR
                assert first_page == page + 1
                assert last_page == page + 1

                return [Image.new("RGB", (100, 100), color="red")]

            monkeypatch.setattr(pdf2image, "convert_from_path", mock_pdf2image)

            # when
            result = generate_preview_service.generate_single_page(
                document_key, task_dir, page
            )

            # then
            assert result[0] == task_dir / f"{page}.png"
            assert result[0].exists()

    def describe_the_generate_whole_document_method():
        def should_call_pdf2image(
            generate_preview_service: GeneratePreviewService,
            monkeypatch: pytest.MonkeyPatch,
        ):
            # given
            document_key = generate_preview_service.get_document_key("test")
            task_dir = Path(
                get_task_dir(generate_preview_service.settings.WORKDIR, "test")
            )

            def mock_pdf2image(document: str, dpi: int):
                assert str(document) == str(document_key)
                assert dpi == 72 * generate_preview_service.settings.DPI_FACTOR

                return [
                    Image.new("RGB", (100, 100), color="red"),
                    Image.new("RGB", (100, 100), color="red"),
                ]

            monkeypatch.setattr(pdf2image, "convert_from_path", mock_pdf2image)

            # when
            result = generate_preview_service.generate_whole_document(
                document_key, task_dir
            )

            # then
            assert result[0] == task_dir / "0.png"
            assert result[1] == task_dir / "1.png"
            assert result[0].exists()
            assert result[1].exists()
