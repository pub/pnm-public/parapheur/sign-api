import pytest
from pathlib import Path
from sign_api.services.count_pages_service import CountPagesService
from reportlab.pdfgen import canvas


def describe_the_count_page_service_class():
    @pytest.fixture
    def count_page_service(base_env):
        count_page_service = CountPagesService()
        return count_page_service

    @pytest.fixture
    def pdf_file():
        path = "/tmp/test.pdf"
        pdf = canvas.Canvas(path, pagesize=(100, 100))
        pdf.drawString(0, 0, "Page 1")
        pdf.showPage()
        pdf.drawString(0, 0, "Page 2")
        pdf.save()
        return path

    def describe_the_count_method():
        def should_returns_a_path_object_with_the_correct_path(
            count_page_service: CountPagesService, pdf_file: str
        ):
            # when
            result = count_page_service.count(Path(pdf_file))

            # then
            assert result == 2
