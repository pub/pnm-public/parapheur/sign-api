from pathlib import Path
from pypdf import PdfReader


class CountPagesService:
    def count(self, document: Path) -> int:
        file = open(document, "rb")
        reader = PdfReader(file)
        return len(reader.pages)
