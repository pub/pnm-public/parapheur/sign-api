import os
import math
from PIL import Image, ImageDraw, ImageFont
from textwrap import wrap
from dependency_injector.wiring import Provide
from logging import Logger


height_factor = 0.35
width_factor = 0.3

min_height = 100
font_size_factor = 0.1
min_font_size = min_height * font_size_factor


class EnhanceSignatureService:
    logger: Logger = Provide["logger"]

    def generate(self, task_dir: str, local_signature_path: str, text: str) -> str:
        self.logger.info(f"generate enhanced sign in dir {task_dir}")
        with Image.open(local_signature_path) as source:
            scale_factor = 1 if source.height > min_height else 2

            self.logger.info(f"opened source img {local_signature_path}")
            font_size = self.get_font_size(source, scale_factor)
            font = self.get_font(font_size)
            self.logger.info(f"font size: {font_size}")

            source_non_transparent = self.remove_transparency(source)
            wider_source = self.get_wider_image(source_non_transparent, scale_factor)
            header_img = self.get_header_image(text, font, font_size, wider_source)
            final_img = self.get_final_img(header_img, wider_source)
            self.logger.info("finished preparing final img")

            result_path = os.path.join(task_dir, f"result.png")
            final_img.save(result_path)
            self.logger.info(f"saved final img {result_path}")
            return result_path

    def get_wider_image(self, source: Image, scale_factor: int) -> Image:
        new_size = (
            math.trunc(source.width * (1 + width_factor) * scale_factor),
            source.height * scale_factor,
        )
        wider_source = Image.new("RGBA", size=new_size, color="white")
        if scale_factor > 1:
            source = source.resize(
                (source.width * scale_factor, source.height * scale_factor),
            )

        box = (math.trunc((wider_source.width - source.width) / 2), 0)
        wider_source.paste(source, box)
        return wider_source

    def get_font_size(self, source: Image, scale_factor: int) -> int:
        return math.trunc(source.height * font_size_factor) * scale_factor

    def get_font(self, font_size: int) -> ImageFont:
        return ImageFont.truetype(
            "sign_api/fonts/Marianne-Regular.woff",
            size=font_size,
        )

    def get_header_image(
        self, text: str, font: ImageFont, font_size: int, wider_source: Image
    ) -> Image:
        max_text_width = math.trunc(wider_source.width / (font_size / 1.8))
        self.logger.info(f"max text width: {max_text_width}")
        wrapped = wrap(text, width=max_text_width)
        nb_lines = len(wrapped)
        wrapped_text = "\n".join(wrapped)
        self.logger.info(f"nb lines: {nb_lines}")

        header_img = Image.new(
            "RGBA",
            size=(
                wider_source.width,
                math.trunc(nb_lines * font_size * (1 + height_factor)),
            ),
            color="white",
        )
        draw = ImageDraw.Draw(header_img)
        draw.multiline_text(
            (wider_source.width / 20, 0),
            wrapped_text,
            font=font,
            fill=(0, 0, 0),
            spacing=0,
        )
        self.logger.info("finished preparing header img")

        return header_img

    def get_final_img(self, header_img: Image, wider_source: Image) -> Image:
        final_img = Image.new(
            "RGBA",
            size=(wider_source.width, wider_source.height + header_img.height),
            color="white",
        )
        final_img.paste(header_img, (0, 0))
        final_img.paste(wider_source, (0, header_img.height))
        return final_img

    # https://stackoverflow.com/questions/35859140/remove-transparency-alpha-from-any-image-using-pil
    def remove_transparency(self, im, bg_colour=(255, 255, 255)) -> Image:
        # Only process if image has transparency (http://stackoverflow.com/a/1963146)
        if im.mode in ("RGBA", "LA") or (im.mode == "P" and "transparency" in im.info):
            # Need to convert to RGBA if LA format due to a bug in PIL (http://stackoverflow.com/a/1963146)
            alpha = im.convert("RGBA").split()[-1]

            # Create a new background image of our matt color.
            # Must be RGBA because paste requires both images have the same format
            # (http://stackoverflow.com/a/8720632  and  http://stackoverflow.com/a/9459208)
            bg = Image.new("RGBA", im.size, bg_colour + (255,))
            bg.paste(im, mask=alpha)
            return bg

        else:
            return im
