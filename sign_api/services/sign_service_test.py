from pathlib import Path
from pypdf import PdfReader
import pytest
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from sign_api.conftest import NullLogger
from sign_api.domain.sign_coordinates import SignCoordinates
from sign_api.services.sign_service import SignService
from sign_api.settings import Settings
from PIL import Image


def describe_the_sign_service_class():
    @pytest.fixture
    def sign_service(base_env):
        sign_service = SignService()
        sign_service.logger = NullLogger()
        sign_service.settings = Settings()
        return sign_service

    @pytest.fixture
    def coordinates():
        return SignCoordinates(
            key="key",
            x=A4[0] / 2,
            y=A4[1] / 2,
            page=0,
            height=100,
            width=100,
        )

    def describe_the_sign_method():
        def should_create_the_sign_pdf(
            sign_service: SignService, coordinates: SignCoordinates
        ):
            # given
            path = Path("/tmp/test.pdf")
            pdf_file = canvas.Canvas(str(path), pagesize=A4)
            pdf_file.drawString(0, 0, "Page 1")
            pdf_file.save()

            coordinates.path = Path("/signature.png")
            image = Image.new("RGB", (100, 100), color="red")
            image.save(coordinates.path)

            workdir = Path("/tmp")

            # when
            result = sign_service.sign(workdir, path, [coordinates])

            # then
            assert result == (workdir / "test.signed.pdf")

        def should_clear_annotations(
            sign_service: SignService, coordinates: SignCoordinates
        ):
            # given
            path = Path("/tmp/test.pdf")
            pdf_file = canvas.Canvas(str(path), pagesize=A4)
            pdf_file.drawString(0, 0, "Page 1")
            pdf_file.textAnnotation("Annotation")
            pdf_file.save()

            file = PdfReader(path)
            assert len(file.pages[0].annotations) == 1

            coordinates.path = Path("/signature.png")
            image = Image.new("RGB", (100, 100), color="red")
            image.save(coordinates.path)

            workdir = Path("/tmp")

            # when
            result = sign_service.sign(workdir, path, [coordinates])

            # then
            assert result == (workdir / "test.signed.pdf")
            with open(result, "rb") as result:
                file = PdfReader(result)
                assert len(file.pages[0].annotations) == 0

    def describe_the_convert_signature_to_pdf_method():
        def should_create_a_pdf_file(
            sign_service: SignService, coordinates: SignCoordinates
        ):
            # given
            workdir = Path("/tmp")
            pagesize = A4
            coordinates.path = Path("/signature.png")
            image = Image.new("RGB", (100, 100), color="red")
            image.save(coordinates.path)

            # when
            pdf_path = sign_service.convert_signature_to_pdf(
                workdir, pagesize, coordinates
            )

            # then
            assert str(pdf_path) == str(workdir / coordinates.get_pdf_filename())
            assert pdf_path.exists()

        def should_use_the_correct_coordinates(
            sign_service: SignService,
            coordinates: SignCoordinates,
            monkeypatch: pytest.MonkeyPatch,
        ):
            # given
            workdir = Path("/tmp")
            pagesize = A4
            coordinates.path = Path("/signature.png")
            image = Image.new("RGB", (100, 100), color="red")
            image.save(coordinates.path)

            def mock_draw_image(
                self,
                image,
                x,
                y,
                width=None,
                height=None,
                **kwargs,
            ):
                # then
                assert x == coordinates.x / sign_service.settings.DPI_FACTOR
                assert (
                    y
                    == coordinates.y / sign_service.settings.DPI_FACTOR
                    - coordinates.height / sign_service.settings.DPI_FACTOR
                )
                assert width == coordinates.width / sign_service.settings.DPI_FACTOR
                assert height == coordinates.height / sign_service.settings.DPI_FACTOR

            monkeypatch.setattr(canvas.Canvas, "drawImage", mock_draw_image)

            # when
            sign_service.convert_signature_to_pdf(workdir, pagesize, coordinates)

    def describe_the_get_pagesize_method():
        def should_returns_the_pagesize(sign_service):
            # given
            path = "/tmp/test.pdf"
            pdf_file = canvas.Canvas(path, pagesize=(100, 200))
            pdf_file.drawString(0, 0, "Page 1")
            pdf_file.save()

            # when
            page_size = sign_service.get_pagesize(path, 0)

            # then
            assert page_size == [100, 200]

        def should_returns_the_pagesize_of_the_requested_page(sign_service):
            # given
            path = "/tmp/test.pdf"
            pdf_file = canvas.Canvas(path, pagesize=(100, 100))
            pdf_file.drawString(0, 0, "Page 1")
            pdf_file.showPage()
            pdf_file.setPageSize((200, 200))
            pdf_file.drawString(0, 0, "Page 2")
            pdf_file.save()

            # when
            page_size = sign_service.get_pagesize(path, 1)

            # then
            assert page_size == [200, 200]

        # This don't works
        # def should_returns_the_pagesize_of_a_rotated_page(sign_service):
        #     # given
        #     path = "/tmp/pipo.pdf"
        #     pdf_file = canvas.Canvas(
        #         path,
        #         pagesize=(100, 200),
        #     )

        #     pdf_file.drawString(0, 0, "Page 1")
        #     pdf_file.setPageRotation(90)
        #     pdf_file.save()

        #     # when
        #     page_size = sign_service.get_pagesize(path, 0)

        #     # then
        #     assert page_size == [200, 100]
