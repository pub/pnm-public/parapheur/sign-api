import subprocess
from pathlib import Path
from sign_api.settings import Settings
from dependency_injector.wiring import Provide
from sign_api.utils import to_md5
import os


class ConvertService:
    settings: Settings = Provide["settings"]

    def get_base_cmd(self) -> str:
        return f"{self.settings.libreoffice_exec} --headless --convert-to pdf:writer_pdf_Export"

    def get_document_key(self, document: str) -> Path:
        file_extension = os.path.splitext(document)[1]
        document_key = (
            Path(self.settings.WORKDIR)
            / "convert"
            / f"{to_md5(document)}{file_extension}"
        )

        if not document_key.exists():
            document_key.parent.mkdir(parents=True, exist_ok=True)

        return document_key

    def convert(self, document: Path, task_dir: Path) -> Path:
        print(f"convert service, {document}, {task_dir}")
        subprocess.run(
            f"{self.get_base_cmd()} --outdir {task_dir} {document}",
            shell=True,
        )
        basename = Path(document).stem
        return task_dir / f"{basename}.pdf"
