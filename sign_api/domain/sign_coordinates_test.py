import pytest
from sign_api.domain.sign_coordinates import SignCoordinates
from sign_api.utils import to_base64


def describe_the_sign_coordinates_class():
    def describe_the_init():
        def should_create_an_instance():
            # given
            key = "key"
            x = -0.4
            y = 1.12
            page = 2
            width = 3.4
            height = 4

            # when
            sign_coordinates = SignCoordinates(
                key=key,
                x=x,
                y=y,
                page=page,
                width=width,
                height=height,
            )

            # then
            assert sign_coordinates is not None
            assert sign_coordinates.key == key
            assert sign_coordinates.x == x
            assert sign_coordinates.y == y
            assert sign_coordinates.page == page
            assert sign_coordinates.width == width
            assert sign_coordinates.height == height

        def describe_with_validation_enabled():
            @pytest.mark.parametrize("key", [1, None, True])
            def should_throw_if_invalid_key(key: str):
                # given
                x = 0.4
                y = 1.12
                page = 2
                width = 3
                height = 4

                # when / then
                with pytest.raises(Exception):
                    SignCoordinates(
                        key=key,
                        x=x,
                        y=y,
                        page=page,
                        width=width,
                        height=height,
                        validate=True,
                    )

            @pytest.mark.parametrize("x", ["toto", None])
            def should_throw_if_invalid_x(x: float):
                # given
                key = "key"
                y = 1.12
                page = 2
                width = 3
                height = 4

                # when / then
                with pytest.raises(Exception):
                    SignCoordinates(
                        key=key,
                        x=x,
                        y=y,
                        page=page,
                        width=width,
                        height=height,
                        validate=True,
                    )

            @pytest.mark.parametrize("y", ["toto", None])
            def should_throw_if_invalid_y(y: float):
                # given
                key = "key"
                x = 1.12
                page = 2
                width = 3
                height = 4

                # when / then
                with pytest.raises(Exception):
                    SignCoordinates(
                        key=key,
                        x=x,
                        y=y,
                        page=page,
                        width=width,
                        height=height,
                        validate=True,
                    )

            @pytest.mark.parametrize("page", ["toto", 1.2, -1, None])
            def should_throw_if_invalid_page(page: int):
                # given
                key = "key"
                x = 1.12
                y = 2
                width = 3
                height = 4

                # when / then
                with pytest.raises(Exception):
                    SignCoordinates(
                        key=key,
                        x=x,
                        y=y,
                        page=page,
                        width=width,
                        height=height,
                        validate=True,
                    )

            @pytest.mark.parametrize("width", ["toto", -1, None])
            def should_throw_if_invalid_width(width: float):
                # given
                key = "key"
                x = 1.12
                y = 2
                page = 3
                height = 4

                # when / then
                with pytest.raises(Exception):
                    SignCoordinates(
                        key=key,
                        x=x,
                        y=y,
                        page=page,
                        width=width,
                        height=height,
                        validate=True,
                    )

            @pytest.mark.parametrize("height", ["toto", -1, None])
            def should_throw_if_invalid_height(height: float):
                # given
                key = "key"
                x = 1.12
                y = 2
                page = 3
                width = 4

                # when / then
                with pytest.raises(Exception):
                    SignCoordinates(
                        key=key,
                        x=x,
                        y=y,
                        page=page,
                        width=width,
                        height=height,
                        validate=True,
                    )

    def describe_the_from_json_static_method():
        def should_create_an_instance():
            # given
            key = "key"
            x = -0.4
            y = 1.12
            page = 2
            width = 3.4
            height = 4

            # when
            sign_coordinates = SignCoordinates.from_json(
                {
                    "key": key,
                    "x": x,
                    "y": y,
                    "page": page,
                    "width": width,
                    "height": height,
                }
            )

            # then
            assert sign_coordinates is not None
            assert sign_coordinates.key == key
            assert sign_coordinates.x == x
            assert sign_coordinates.y == y
            assert sign_coordinates.page == page
            assert sign_coordinates.width == width
            assert sign_coordinates.height == height

        def describe_with_validation_enabled():
            @pytest.mark.parametrize("key", [1, None, True])
            def should_throw_if_invalid_key(key: str):
                # given
                x = 0.4
                y = 1.12
                page = 2
                width = 3
                height = 4

                # when / then
                with pytest.raises(Exception):
                    SignCoordinates.from_json(
                        {
                            "key": key,
                            "x": x,
                            "y": y,
                            "page": page,
                            "width": width,
                            "height": height,
                        },
                        validate=True,
                    )

            @pytest.mark.parametrize("x", ["toto", None])
            def should_throw_if_invalid_x(x: float):
                # given
                key = "key"
                y = 1.12
                page = 2
                width = 3
                height = 4

                # when / then
                with pytest.raises(Exception):
                    SignCoordinates.from_json(
                        {
                            "key": key,
                            "x": x,
                            "y": y,
                            "page": page,
                            "width": width,
                            "height": height,
                        },
                        validate=True,
                    )

            @pytest.mark.parametrize("y", ["toto", None])
            def should_throw_if_invalid_y(y: float):
                # given
                key = "key"
                x = 1.12
                page = 2
                width = 3
                height = 4

                # when / then
                with pytest.raises(Exception):
                    SignCoordinates.from_json(
                        {
                            "key": key,
                            "x": x,
                            "y": y,
                            "page": page,
                            "width": width,
                            "height": height,
                        },
                        validate=True,
                    )

            @pytest.mark.parametrize("page", ["toto", 1.2, -1, None])
            def should_throw_if_invalid_page(page: int):
                # given
                key = "key"
                x = 1.12
                y = 2
                width = 3
                height = 4

                # when / then
                with pytest.raises(Exception):
                    SignCoordinates.from_json(
                        {
                            "key": key,
                            "x": x,
                            "y": y,
                            "page": page,
                            "width": width,
                            "height": height,
                        },
                        validate=True,
                    )

            @pytest.mark.parametrize("width", ["toto", -1, None])
            def should_throw_if_invalid_width(width: float):
                # given
                key = "key"
                x = 1.12
                y = 2
                page = 3
                height = 4

                # when / then
                with pytest.raises(Exception):
                    SignCoordinates.from_json(
                        {
                            "key": key,
                            "x": x,
                            "y": y,
                            "page": page,
                            "width": width,
                            "height": height,
                        },
                        validate=True,
                    )

            @pytest.mark.parametrize("height", ["toto", -1, None])
            def should_throw_if_invalid_height(height: float):
                # given
                key = "key"
                x = 1.12
                y = 2
                page = 3
                width = 4

                # when / then
                with pytest.raises(Exception):
                    SignCoordinates.from_json(
                        {
                            "key": key,
                            "x": x,
                            "y": y,
                            "page": page,
                            "width": width,
                            "height": height,
                        },
                        validate=True,
                    )

    def describe_the_repr():
        def should_return_a_string_representation():
            # given
            key = "key"
            x = -0.4
            y = 1.12
            page = 2
            width = 3.4
            height = 4

            # when
            sign_coordinates = SignCoordinates(
                key=key,
                x=x,
                y=y,
                page=page,
                width=width,
                height=height,
            )
            result = str(sign_coordinates)

            # then
            assert (
                result
                == f"<SignCoordinates key={key} x={x} y={y} page={page} width={width} height={height}>"
            )

    def describe_the_get_pdf_filename():
        def should_return_a_filename():
            # given
            key = "key"
            x = -0.4
            y = 1.12
            page = 2
            width = 3.4
            height = 4

            # when
            sign_coordinates = SignCoordinates(
                key=key,
                x=x,
                y=y,
                page=page,
                width=width,
                height=height,
            )
            result = sign_coordinates.get_pdf_filename()

            # then
            assert (
                result
                == f"signature_{to_base64(key)[:100]}_{x}_{y}_{page}_{width}_{height}.pdf"
            )

        def should_return_different_filename_for_different_coordinates():
            # given
            key = "key"
            x = -0.4
            y = 1.12
            page = 2
            width = 3.4
            height = 4

            sign_coordinates1 = SignCoordinates(
                key=key,
                x=x,
                y=y,
                page=page,
                width=width,
                height=height,
            )

            sign_coordinates2 = SignCoordinates(
                key=key,
                x=x + 100,
                y=y - 10,
                page=page,
                width=width,
                height=height,
            )

            sign_coordinates3 = SignCoordinates(
                key=key,
                x=x,
                y=y,
                page=page,
                width=width,
                height=height,
            )

            # when
            result1 = sign_coordinates1.get_pdf_filename()
            result2 = sign_coordinates2.get_pdf_filename()
            result3 = sign_coordinates3.get_pdf_filename()

            # then
            assert result1 != result2
            assert result1 == result3
