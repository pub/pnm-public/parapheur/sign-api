from cerberus import Validator
from pathlib import Path

from sign_api.utils import to_base64, without_keys


class SignCoordinates:
    path: Path
    schema = {
        "key": {"type": "string", "required": True},
        "x": {
            "type": "float",
            "required": True,
        },
        "y": {
            "type": "float",
            "required": True,
        },
        "page": {"type": "integer", "required": True, "min": 0},
        "width": {"type": "float", "required": True, "min": 0},
        "height": {"type": "float", "required": True, "min": 0},
    }

    def __init__(
        self,
        key: str,
        x: int,
        y: int,
        page: int,
        width: int,
        height: int,
        validate: bool = False,
    ):
        self.key = key
        self.x = x
        self.y = y
        self.page = page
        self.width = width
        self.height = height
        self.path = None

        if validate and not Validator(self.schema).validate(
            without_keys(self.__dict__, ["path"])
        ):
            raise Exception("Invalid coordinates")

    def __repr__(self) -> str:
        return f"<SignCoordinates key={self.key} x={self.x} y={self.y} page={self.page} width={self.width} height={self.height}>"

    def __str__(self) -> str:
        return self.__repr__()

    def get_pdf_filename(self):
        return f"signature_{to_base64(self.key)[:100]}_{self.x}_{self.y}_{self.page}_{self.width}_{self.height}.pdf"

    @staticmethod
    def from_json(data: dict, validate: bool = False) -> "SignCoordinates":
        return SignCoordinates(
            key=data["key"],
            x=data["x"],
            y=data["y"],
            page=data["page"],
            width=data["width"],
            height=data["height"],
            validate=validate,
        )
