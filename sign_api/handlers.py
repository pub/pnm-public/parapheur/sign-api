import json
from flask import request


from dependency_injector.wiring import inject, Provide
from werkzeug import exceptions


def parse_body():
    request.body = request.get_json(force=True, silent=True) or {}


@inject
def handle_exception(logger=Provide["logger"]):
    def _handle_exception(e: Exception):
        logger.exception(e)
        if isinstance(e, exceptions.HTTPException):
            response = e.get_response()
            response.data = json.dumps(
                {
                    "code": e.code,
                    "name": e.name,
                    "description": e.description,
                }
            )
            response.content_type = "application/json"
            return response

        return {
            "code": 500,
            "name": "Internal Server Error",
            "description": str(e),
        }, 500

    return _handle_exception
