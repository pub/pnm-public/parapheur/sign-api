from typing import Protocol
from flask import Flask
import pytest
from pyfakefs.fake_filesystem import FakeFilesystem

from sign_api.handlers import parse_body


class MockEnv(Protocol):
    def __call__(self, **envvars: str):
        ...


@pytest.fixture
def mock_env(monkeypatch: pytest.MonkeyPatch) -> MockEnv:
    def _mock_env(**envvars: str):
        for var, val in envvars.items():
            monkeypatch.setenv(var, val)

    yield _mock_env


@pytest.fixture
def base_env(monkeypatch: pytest.MonkeyPatch, fs: FakeFilesystem) -> None:
    monkeypatch.setenv("MINIO_ACCESS_KEY", "access_key")
    monkeypatch.setenv("MINIO_SECRET_KEY", "secret_key")


class NullLogger(object):
    def __getattr__(self, name):
        return lambda *args, **kwargs: NullLogger


@pytest.fixture
def app():
    app = Flask(__name__)
    app.before_request(parse_body)
    return app
