import os
import pytest

from sign_api.utils import get_task_dir, to_base64, without_keys


def describe_the_get_task_dir_function():
    @pytest.fixture(autouse=True)
    def before_each(base_env):
        pass

    def should_return_the_task_dir():
        # given
        workdir = "/tmp/sign_api"
        task_id = "123"

        # when
        task_dir = get_task_dir(workdir, task_id)

        # then
        assert task_dir == "/tmp/sign_api/123"

    def should_create_the_task_dir():
        # given
        workdir = "/tmp/sign_api"
        task_id = "123"
        assert not os.path.exists("/tmp/sign_api/123")

        # when
        task_dir = get_task_dir(workdir, task_id)

        # then
        assert task_dir == "/tmp/sign_api/123"
        assert os.path.exists(task_dir)


def describe_the_without_keys_function():
    def should_return_the_dict_without_the_given_keys():
        # given
        d = {"a": 1, "b": 2, "c": 3}
        keys = ["a", "c"]

        # when
        result = without_keys(d, keys)

        # then
        assert result == {"b": 2}

    def should_return_the_dict_if_no_keys_are_given():
        # given
        d = {"a": 1, "b": 2, "c": 3}
        keys = []

        # when
        result = without_keys(d, keys)

        # then
        assert result == {"a": 1, "b": 2, "c": 3}


def describe_the_to_base64_function():
    def should_return_the_base64_encoded_string():
        # given
        string = "test"

        # when
        result = to_base64(string)

        # then
        assert result == "dGVzdA=="
