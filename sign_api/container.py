from dependency_injector import containers, providers
from flask import request
from sign_api.logging import get_logger
from sign_api.services.generate_preview_service import GeneratePreviewService
from sign_api.settings import Settings
from sign_api.services.minio_service import MinioService
from sign_api.services.sign_service import SignService
from sign_api.services.count_pages_service import CountPagesService
from sign_api.services.enhance_signature_service import EnhanceSignatureService
from sign_api.services.convert_service import ConvertService


class Container(containers.DeclarativeContainer):
    wiring_config = containers.WiringConfiguration(
        packages=[
            "sign_api",
            "sign_api.services",
            "sign_api.controllers",
            "sign_api.decorators",
        ],
    )

    logger = providers.Callable(get_logger)
    settings = providers.Singleton(Settings)
    minio_service = providers.Singleton(MinioService)
    sign_service = providers.Singleton(SignService)
    count_pages_service = providers.Singleton(CountPagesService)
    generate_preview_service = providers.Singleton(GeneratePreviewService)
    enhance_signature_service = providers.Singleton(EnhanceSignatureService)
    convert_service = providers.Singleton(ConvertService)

    request = providers.Callable(lambda: request)
    body = providers.Callable(lambda: request.body)
    query = providers.Callable(lambda: request.args)
