from datetime import datetime, timedelta
from pathlib import Path
from freezegun import freeze_time
from pyfakefs.fake_filesystem import FakeFilesystem
from sign_api.conftest import NullLogger

from sign_api.scheduler import clean_work_dir, create_scheduler
from sign_api.settings import Settings

from unittest.mock import patch


def describe_the_clean_work_dir_function():
    def should_clean_the_work_dir(base_env, fs: FakeFilesystem):
        # given
        settings = Settings()
        workdir = settings.WORKDIR

        fs.create_file(f"{workdir}/file.txt")
        fs.create_dir(f"{workdir}/folder")
        fs.create_file(f"{workdir}/folder/file.txt")
        assert Path(f"{workdir}/file.txt").exists() is True
        assert Path(f"{workdir}/folder/file.txt").exists() is True

        # when
        with freeze_time(timedelta(hours=2)):
            clean_work_dir(NullLogger(), settings)

        # then
        assert Path(f"{workdir}/file.txt").exists() is False
        assert Path(f"{workdir}/folder/file.txt").exists() is False
        assert Path(f"{workdir}/folder").exists() is False
        assert Path(workdir).exists() is True

    def should_only_delete_files_older_than_1h(base_env, fs: FakeFilesystem):
        # given
        settings = Settings()
        workdir = settings.WORKDIR

        fs.create_file(f"{workdir}/file_old.txt")
        with freeze_time(timedelta(hours=2)):
            fs.create_file(f"{workdir}/file_young.txt")

            # when
            clean_work_dir(NullLogger(), settings)

        # then
        assert Path(f"{workdir}/file_old.txt").exists() is False
        assert Path(f"{workdir}/file_young.txt").exists() is True


def describe_the_create_scheduler_function():
    def should_create_a_scheduler():
        with patch("sign_api.scheduler.clean_work_dir") as mock_clean_work_dir:
            with freeze_time("2023-01-01 23:59:59") as frozen_datetime:
                # given
                scheduler = create_scheduler()
                original_time = frozen_datetime()

                # when
                while (
                    not mock_clean_work_dir.called
                    and datetime.now()
                    < original_time
                    + timedelta(
                        seconds=3000
                    )  # If the test takes more than 3000 loops, it's probably broken
                ):
                    frozen_datetime.tick(delta=timedelta(seconds=1))
                    scheduler.wakeup()

                # then
                mock_clean_work_dir.assert_called()

        # when
