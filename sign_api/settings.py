import os


class Settings:
    def __init__(self):
        self.WORKDIR = os.environ.get("WORKDIR", "/tmp/sign_api")
        self.SECRET_KEY = os.environ.get("SECRET_KEY", "secret_key")
        self.DEBUG = os.environ.get("DEBUG", "False") == "True"
        self.DPI_FACTOR = int(os.environ.get("DPI_FACTOR", "2"))
        self.MINIO = {
            "ENDPOINT": os.environ.get("MINIO_ENDPOINT", "localhost:9000"),
            "SECURE": os.environ.get("MINIO_SECURE", "false").lower() == "true",
            "BUCKET": os.environ.get("MINIO_BUCKET", "sign-api"),
            "ACCESS_KEY": os.environ.get("MINIO_ACCESS_KEY"),
            "SECRET_KEY": os.environ.get("MINIO_SECRET_KEY"),
            "REGION": os.environ.get("MINIO_REGION", None),
            "PROXY": os.environ.get("MINIO_PROXY", None),
        }
        self.libreoffice_exec = os.environ.get("LIBREOFFICE_EXEC", "soffice")

        if not os.path.exists(self.WORKDIR):
            os.makedirs(self.WORKDIR, exist_ok=True)

        if not self.MINIO["ACCESS_KEY"] or not self.MINIO["SECRET_KEY"]:
            raise Exception("MINIO_ACCESS_KEY or MINIO_SECRET_KEY is not set")
