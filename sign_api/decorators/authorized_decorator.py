from functools import wraps
from http import HTTPStatus
from flask import request, jsonify
from dependency_injector.wiring import inject, Provide

from sign_api.settings import Settings


def authorized(func):
    @wraps(func)
    @inject
    def wrapper(*args, settings: Settings = Provide["settings"], **kwargs):
        secret_key = settings.SECRET_KEY

        if not request.headers.get("X-Api-Key", None) == secret_key:
            return (
                jsonify({"error": "Missing or invalid authorization"}),
                HTTPStatus.UNAUTHORIZED,
            )
        return func(*args, **kwargs)

    return wrapper
