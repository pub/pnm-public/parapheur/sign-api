from functools import wraps
from http import HTTPStatus
from dependency_injector.wiring import inject, Provide
from cerberus import Validator


def validate(schema: dict):
    validator = Validator(schema)

    def decorator(func):
        @wraps(func)
        @inject
        def wrapper(*args, body: dict = Provide["body"], **kwargs):
            if not validator.validate(body):
                return {"error": validator.errors}, HTTPStatus.BAD_REQUEST
            return func(*args, **kwargs)

        return wrapper

    return decorator
