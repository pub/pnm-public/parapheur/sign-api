from flask import Flask
import pytest
from sign_api.container import Container

from sign_api.decorators.validate_decorator import validate


def describe_the_validate_decorator():
    def should_block_when_provided_body_is_invalid(
        app: Flask, monkeypatch: pytest.MonkeyPatch, base_env
    ):
        # given
        @app.post("/test")
        @validate(
            {"document": {"type": "string", "required": True}},
        )
        def test_route():
            return "pipo"

        container = Container()
        container.wire(modules=[__name__])

        # when
        response = app.test_client().post("/test", json={})

        # then
        assert response.status_code == 400
        assert response.json == {"error": {"document": ["required field"]}}
        container.unwire()

    def should_allow_when_provided_body_is_valid(
        app: Flask, monkeypatch: pytest.MonkeyPatch, base_env
    ):
        # given
        @app.post("/test")
        @validate(
            {"document": {"type": "string", "required": True}},
        )
        def test_route():
            return "pipo"

        container = Container()
        container.wire(modules=[__name__])

        # when
        response = app.test_client().post("/test", json={"document": "toto"})

        # then
        assert response.status_code == 200
        container.unwire()
