from flask import Flask
import pytest
from sign_api.decorators.authorized_decorator import authorized
from sign_api.container import Container


def describe_the_authorized_decorator():
    def should_block_request_without_api_key(
        app: Flask, monkeypatch: pytest.MonkeyPatch, base_env
    ):
        # given
        @app.route("/test")
        @authorized
        def test_route():
            return "pipo"

        monkeypatch.setenv("SECRET_KEY", "toto")
        container = Container()
        container.wire(
            modules=[
                __name__,
                "sign_api.services.minio_service",
            ],
        )

        # when
        response = app.test_client().get("/test")
        # test_route()

        # then
        assert response.status_code == 401
        assert response.json == {"error": "Missing or invalid authorization"}
        container.unwire()

    def should_block_request_with_invalid_api_key(
        app: Flask, monkeypatch: pytest.MonkeyPatch, base_env
    ):
        # given
        @app.route("/test")
        @authorized
        def test_route():
            return "pipo"

        monkeypatch.setenv("SECRET_KEY", "toto")
        container = Container()
        container.wire(modules=[__name__])

        # when
        response = app.test_client().get("/test", headers={"X-Api-Key": "wrong_value"})

        # then
        assert response.status_code == 401
        assert response.json == {"error": "Missing or invalid authorization"}
        container.unwire()

    def should_allow_request_with_valid_api_key(
        app: Flask, monkeypatch: pytest.MonkeyPatch, base_env
    ):
        # given
        @app.route("/test")
        @authorized
        def test_route():
            return "pipo"

        monkeypatch.setenv("SECRET_KEY", "toto")
        container = Container()
        container.wire(modules=[__name__])

        # when
        response = app.test_client().get("/test", headers={"X-Api-Key": "toto"})

        # then
        assert response.status_code == 200
        container.unwire()
