from pathlib import Path
from unittest.mock import Mock
from flask import Flask
import pytest
from sign_api.container import Container
from sign_api.controllers.count_pages_controller import count_pages_controller
from sign_api.services.minio_service import MinioService
from pyfakefs.fake_filesystem import FakeFilesystem
from reportlab.pdfgen import canvas


def describe_the_count_pages_controller():
    @pytest.fixture
    def mock_minio_service(monkeypatch: pytest.MonkeyPatch, fs: FakeFilesystem):
        def mock_get_file(source_key: str, target_path: Path) -> Path:
            pdf = canvas.Canvas(str(target_path), pagesize=(100, 100))
            pdf.drawString(0, 0, "Page 1")
            pdf.showPage()
            pdf.drawString(0, 0, "Page 2")
            pdf.save()
            return Path(target_path)

        mock_minio_service: Mock[MinioService] = Mock(spec=MinioService)

        mock_minio_service.get_file = mock_get_file
        return mock_minio_service

    @pytest.fixture
    def count_pages_app(
        base_env, app: Flask, monkeypatch: pytest.MonkeyPatch, mock_minio_service
    ):
        app.register_blueprint(count_pages_controller)
        monkeypatch.setenv("SECRET_KEY", "secret_key")

        container = Container()

        container.minio_service.override(mock_minio_service)

        container.wire(
            modules=[
                __name__,
                "sign_api.decorators.authorized_decorator",
                "sign_api.services.count_pages_service",
                "sign_api.controllers.count_pages_controller",
            ],
        )
        yield app
        container.unwire()

    def describe_the_count_pages_route():
        def should_return_the_correct_nb_of_pages(count_pages_app: Flask):
            response = count_pages_app.test_client().post(
                "/count-pages",
                headers={"X-Api-Key": "secret_key"},
                json={"document": "test.pdf"},
            )

            assert response.status_code == 200
            assert response.json == {"nbPages": 2}

        def should_return_401_if_no_auth_provided(count_pages_app: Flask):
            response = count_pages_app.test_client().post(
                "/count-pages",
                json={"document": "test.pdf"},
            )

            assert response.status_code == 401

        def should_return_401_if_invalid_auth_provided(count_pages_app: Flask):
            response = count_pages_app.test_client().post(
                "/count-pages",
                headers={"X-Api-Key": "toto"},
                json={"document": "test.pdf"},
            )

            assert response.status_code == 401

        def should_return_400_if_invalid_body_provided(count_pages_app: Flask):
            response = count_pages_app.test_client().post(
                "/count-pages",
                headers={"X-Api-Key": "toto"},
                json={"toto": "test.pdf"},
            )

            assert response.status_code == 401
