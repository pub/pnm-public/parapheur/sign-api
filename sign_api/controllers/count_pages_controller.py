from dependency_injector.wiring import inject, Provide
from flask import blueprints
from logging import Logger
from pathlib import Path
from sign_api.decorators.authorized_decorator import authorized
from sign_api.decorators.validate_decorator import validate
from sign_api.services.count_pages_service import CountPagesService
from sign_api.services.minio_service import MinioService
from sign_api.settings import Settings
import uuid
from sign_api.utils import get_task_dir


count_pages_controller = blueprints.Blueprint("count-pages", __name__)


@count_pages_controller.post("/count-pages")
@authorized
@validate(
    {
        "document": {"type": "string", "required": True},
    }
)
@inject
def get_nb_pages(
    settings: Settings = Provide["settings"],
    logger: Logger = Provide["logger"],
    minio_service: MinioService = Provide["minio_service"],
    count_pages_service: CountPagesService = Provide["count_pages_service"],
    body: dict = Provide["body"],
):
    logger.info(
        f"Compute nb pages for doc {body['document']}",
        extra={"document": body["document"]},
    )
    task_dir = Path(get_task_dir(settings.WORKDIR, uuid.uuid4().hex))
    document = minio_service.get_file(body["document"], task_dir / "source_doc.pdf")
    logger.info(f"Downloaded file to workdir {task_dir}")
    count = count_pages_service.count(document)
    logger.info(f"Found {count} pages for {body['document']}")
    return {"nbPages": count}
