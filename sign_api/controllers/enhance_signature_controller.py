import os
from dependency_injector.wiring import inject, Provide
from flask import blueprints
from logging import Logger
from pathlib import Path
from sign_api.decorators.authorized_decorator import authorized
from sign_api.decorators.validate_decorator import validate
from sign_api.services.minio_service import MinioService
from sign_api.services.enhance_signature_service import EnhanceSignatureService
from sign_api.settings import Settings
import uuid

from sign_api.utils import get_task_dir


enhance_signature_controller = blueprints.Blueprint(
    "enhance_signature_controller", __name__
)


@enhance_signature_controller.post("/enhance_signature")
@authorized
@validate(
    {
        "signature_key": {"type": "string", "required": True},
        "text": {
            "type": "string",
            "required": True,
        },
        "destination": {"type": "string", "required": True},
    }
)
@inject
def generate_enhanced_signature(
    settings: Settings = Provide["settings"],
    logger: Logger = Provide["logger"],
    minio_service: MinioService = Provide["minio_service"],
    enhance_signature_service: EnhanceSignatureService = Provide[
        "enhance_signature_service"
    ],
    body: dict = Provide["body"],
):
    task_dir = Path(get_task_dir(settings.WORKDIR, uuid.uuid4().hex))
    logger.info(
        f"Start generating sign image in task dir: {task_dir}",
        extra={"task_dir": task_dir},
    )
    signature_key = body.get("signature_key")
    target_name = f"signature.{signature_key.split('.')[-1]}"
    target_path = os.path.join(task_dir, target_name)
    minio_service.get_file(signature_key, target_path)
    logger.info(
        f"Signature {signature_key} was downloaded",
        extra={"task_dir": task_dir},
    )
    result = enhance_signature_service.generate(task_dir, target_path, body.get("text"))
    logger.info(f"Successfully generated enhanced signature {result}")
    destination = body.get("destination")
    minio_service.put_file(result, destination)
    return {"url": minio_service.get_signed_url(destination)}
