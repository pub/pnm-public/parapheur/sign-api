from pathlib import Path
from unittest.mock import Mock
from flask import Flask
import pytest
from sign_api.container import Container
from sign_api.controllers.sign_controller import sign_controller
from sign_api.domain.sign_coordinates import SignCoordinates
from sign_api.services.minio_service import MinioService
from pyfakefs.fake_filesystem import FakeFilesystem
from reportlab.pdfgen import canvas

from sign_api.services.sign_service import SignService
from PIL import Image


def describe_the_sign_controller():
    @pytest.fixture
    def mock_minio_service(monkeypatch: pytest.MonkeyPatch, fs: FakeFilesystem):
        def mock_get_file(source_key: str, target_path: Path) -> Path:
            if source_key.endswith(".pdf"):
                pdf = canvas.Canvas(str(target_path), pagesize=(100, 100))
                pdf.drawString(0, 0, "Page 1")
                pdf.showPage()
                pdf.drawString(0, 0, "Page 2")
                pdf.save()
                return Path(target_path)
            elif source_key.endswith(".png"):
                Image.new("RGB", (100, 100), color="red").save(target_path)
                return Path(target_path)

        mock_minio_service: Mock[MinioService] = Mock(spec=MinioService)

        mock_minio_service.get_file = mock_get_file
        mock_minio_service.put_file.return_value = ["ok"]
        mock_minio_service.get_signed_url.return_value = "signed_url"

        return mock_minio_service

    @pytest.fixture
    def mock_sign_service(monkeypatch: pytest.MonkeyPatch, fs: FakeFilesystem):
        def mock_sign(
            workdir: Path,
            pdf_path: Path,
            signatures: list[SignCoordinates],
        ):
            return workdir / f"{pdf_path.stem}.signed.pdf"

        mock_sign_service: Mock[MinioService] = Mock(spec=SignService)
        mock_sign_service.sign = mock_sign

        return mock_sign_service

    @pytest.fixture
    def sign_app(
        base_env,
        app: Flask,
        monkeypatch: pytest.MonkeyPatch,
        mock_minio_service: MinioService,
        mock_sign_service: SignService,
    ):
        app.register_blueprint(sign_controller)
        monkeypatch.setenv("SECRET_KEY", "secret_key")

        container = Container()

        container.minio_service.override(mock_minio_service)
        container.sign_service.override(mock_sign_service)

        container.wire(
            modules=[
                __name__,
                "sign_api.decorators.authorized_decorator",
                "sign_api.controllers.sign_controller",
            ],
        )
        yield app
        container.unwire()

    def describe_the_sign_route():
        def should_sign(sign_app):
            response = sign_app.test_client().post(
                "/sign",
                headers={"X-Api-Key": "secret_key"},
                json={
                    "document": "document.pdf",
                    "destination": "signed_document.pdf",
                    "signatures": [
                        {
                            "key": "signature.png",
                            "page": 0,
                            "x": 0,
                            "y": 0,
                            "width": 100,
                            "height": 100,
                        }
                    ],
                },
            )

            assert response.status_code == 200
            assert response.json == {"url": "signed_url"}

        def should_return_401_when_no_auth_provided(sign_app):
            response = sign_app.test_client().post(
                "/sign",
                json={
                    "document": "document.pdf",
                    "destination": "signed_document.pdf",
                    "signatures": [
                        {
                            "key": "signature.png",
                            "page": 0,
                            "x": 0,
                            "y": 0,
                            "width": 100,
                            "height": 100,
                        }
                    ],
                },
            )

            assert response.status_code == 401

        def should_return_401_when_invalid_auth_provided(sign_app):
            response = sign_app.test_client().post(
                "/sign",
                headers={"X-Api-Key": "invalid"},
                json={
                    "document": "document.pdf",
                    "destination": "signed_document.pdf",
                    "signatures": [
                        {
                            "key": "signature.png",
                            "page": 0,
                            "x": 0,
                            "y": 0,
                            "width": 100,
                            "height": 100,
                        }
                    ],
                },
            )

            assert response.status_code == 401

        def should_return_400_when_invalid_body_provided(sign_app):
            response = sign_app.test_client().post(
                "/sign",
                headers={"X-Api-Key": "secret_key"},
                json={
                    "document": "document.pdf",
                    "signatures": [
                        {
                            "key": "signature.png",
                            "page": 0,
                            "x": 0,
                            "y": 0,
                            "width": 100,
                            "height": 100,
                        }
                    ],
                },
            )

            assert response.status_code == 400
