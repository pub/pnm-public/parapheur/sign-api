from flask import blueprints
from sign_api.decorators.authorized_decorator import authorized
from sign_api.decorators.validate_decorator import validate
from dependency_injector.wiring import inject, Provide
from sign_api.settings import Settings
from sign_api.services.minio_service import MinioService
from sign_api.services.convert_service import ConvertService
from logging import Logger
from pathlib import Path
import uuid
from sign_api.utils import get_task_dir


convert_controller = blueprints.Blueprint("convert", __name__)


@convert_controller.post("/convert")
@authorized
@validate(
    {
        "document": {"type": "string", "required": True},
        "destination": {"type": "string", "required": True},
    }
)
@inject
def convert(
    settings: Settings = Provide["settings"],
    logger: Logger = Provide["logger"],
    minio_service: MinioService = Provide["minio_service"],
    convert_service: ConvertService = Provide["convert_service"],
    body: dict = Provide["body"],
):
    doc = body.get("document")
    document_key = convert_service.get_document_key(doc)
    task_dir = Path(get_task_dir(settings.WORKDIR, uuid.uuid4().hex))
    logger.info(f"Converting document {doc} in task dir: {task_dir}")

    if not document_key.exists():
        minio_service.get_file(doc, document_key)

    converted_file = convert_service.convert(document_key, task_dir)

    minio_service.put_file(converted_file, body.get("destination"))
    return ""
