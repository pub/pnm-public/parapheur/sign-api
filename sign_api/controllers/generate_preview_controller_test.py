from pathlib import Path
from unittest.mock import Mock
from flask import Flask
import pytest
from sign_api.container import Container
from sign_api.controllers.generate_preview_controller import generate_preview_controller
from sign_api.services.generate_preview_service import GeneratePreviewService
from sign_api.services.minio_service import MinioService
from pyfakefs.fake_filesystem import FakeFilesystem
from reportlab.pdfgen import canvas


def describe_the_generate_preview_controller():
    @pytest.fixture
    def mock_minio_service(monkeypatch: pytest.MonkeyPatch, fs: FakeFilesystem):
        def mock_get_file(source_key: str, target_path: Path) -> Path:
            pdf = canvas.Canvas(str(target_path), pagesize=(100, 100))
            pdf.drawString(0, 0, "Page 1")
            pdf.showPage()
            pdf.drawString(0, 0, "Page 2")
            pdf.save()
            return Path(target_path)

        mock_minio_service: Mock[MinioService] = Mock(spec=MinioService)

        mock_minio_service.get_file = mock_get_file
        mock_minio_service.put_file.return_value = ["ok"]
        mock_minio_service.get_signed_url.return_value = "signed_url"

        return mock_minio_service

    @pytest.fixture
    def mock_generate_preview_service(
        monkeypatch: pytest.MonkeyPatch, fs: FakeFilesystem
    ):
        def mock_generate_single_page(
            document: Path,
            task_dir: Path,
            page: int,
        ) -> Path:
            return [task_dir / f"{page}.png"]

        def mock_generate_whole_document(
            document: Path,
            task_dir: Path,
        ) -> Path:
            return [task_dir / f"{0}.png", task_dir / f"{1}.png"]

        mock_generate_preview_service: Mock[MinioService] = Mock(
            spec=GeneratePreviewService
        )
        mock_generate_preview_service.get_document_key.return_value = Path("toto")
        mock_generate_preview_service.generate_single_page = mock_generate_single_page
        mock_generate_preview_service.generate_whole_document = (
            mock_generate_whole_document
        )

        return mock_generate_preview_service

    @pytest.fixture
    def generate_preview_app(
        base_env,
        app: Flask,
        monkeypatch: pytest.MonkeyPatch,
        mock_minio_service: MinioService,
        mock_generate_preview_service: GeneratePreviewService,
    ):
        app.register_blueprint(generate_preview_controller)
        monkeypatch.setenv("SECRET_KEY", "secret_key")

        container = Container()

        container.minio_service.override(mock_minio_service)
        container.generate_preview_service.override(mock_generate_preview_service)

        container.wire(
            modules=[
                __name__,
                "sign_api.decorators.authorized_decorator",
                "sign_api.services.count_pages_service",
                "sign_api.controllers.generate_preview_controller",
            ],
        )
        yield app
        container.unwire()

    def describe_the_generate_preview_route():
        def should_generate_whole_document(generate_preview_app):
            response = generate_preview_app.test_client().post(
                "/generate_preview",
                headers={"X-Api-Key": "secret_key"},
                json={
                    "document": "document.pdf",
                    "prefix": "prefix/",
                },
            )

            assert response.status_code == 200
            assert response.json == {
                "urls": [
                    "signed_url",
                    "signed_url",
                ],
            }

        def should_generate_single_page(generate_preview_app):
            response = generate_preview_app.test_client().post(
                "/generate_preview",
                headers={"X-Api-Key": "secret_key"},
                json={"document": "document.pdf", "prefix": "prefix/", "page": 1},
            )

            assert response.status_code == 200
            assert response.json == {
                "urls": [
                    "signed_url",
                ],
            }

        def should_return_401_when_no_auth_is_provided(generate_preview_app):
            response = generate_preview_app.test_client().post(
                "/generate_preview",
                json={
                    "document": "document.pdf",
                    "prefix": "prefix/",
                },
            )

            assert response.status_code == 401

        def should_return_401_when_invalid_auth_is_provided(generate_preview_app):
            response = generate_preview_app.test_client().post(
                "/generate_preview",
                headers={"X-Api-Key": "invalid"},
                json={
                    "document": "document.pdf",
                    "prefix": "prefix/",
                },
            )

            assert response.status_code == 401

        def should_return_400_when_invalid_body_is_provided(generate_preview_app):
            response = generate_preview_app.test_client().post(
                "/generate_preview",
                headers={"X-Api-Key": "secret_key"},
                json={
                    "document": "document.pdf",
                },
            )

            assert response.status_code == 400
