from dependency_injector.wiring import inject, Provide
from flask import blueprints
from logging import Logger
from pathlib import Path
from sign_api.decorators.authorized_decorator import authorized
from sign_api.decorators.validate_decorator import validate
from sign_api.domain.sign_coordinates import SignCoordinates
from sign_api.services.generate_preview_service import GeneratePreviewService
from sign_api.services.minio_service import MinioService
from sign_api.services.sign_service import SignService
from sign_api.settings import Settings
import uuid

from sign_api.utils import get_task_dir, to_base64


sign_controller = blueprints.Blueprint("sign", __name__)


@sign_controller.post("/sign")
@authorized
@validate(
    {
        "document": {"type": "string", "required": True},
        "destination": {"type": "string", "required": True},
        "signatures": {
            "type": "list",
            "schema": {"type": "dict", "schema": SignCoordinates.schema},
        },
    }
)
@inject
def sign(
    settings: Settings = Provide["settings"],
    minio_service: MinioService = Provide["minio_service"],
    generate_preview_service: GeneratePreviewService = Provide[
        "generate_preview_service"
    ],
    sign_service: SignService = Provide["sign_service"],
    logger: Logger = Provide["logger"],
    body: dict = Provide["body"],
):
    task_dir = Path(get_task_dir(settings.WORKDIR, uuid.uuid4().hex))
    logger.info(f"Start signing in task dir: {task_dir}", extra={"task_dir": task_dir})

    document = minio_service.get_file(body["document"], task_dir / "source_doc.pdf")
    logger.info(
        f"Document: {body['document']} was downloaded", extra={"task_dir": task_dir}
    )

    signatures = [
        SignCoordinates.from_json(signature) for signature in body["signatures"]
    ]
    for signature in signatures:
        signature.path = (
            task_dir / f"{to_base64(signature.key)}.{signature.key.split('.')[-1]}"
        )
        if not signature.path.exists():
            minio_service.get_file(signature.key, str(signature.path))
            logger.info(
                f"Signature: {signature.key} was downloaded",
                extra={"task_dir": task_dir},
            )

    logger.info(
        f"Start signing document: {document} with signature ",
        extra={"task_dir": task_dir},
    )
    signed_document = sign_service.sign(
        task_dir,
        document,
        signatures,
    )
    logger.info(f"Document: {signed_document} was signed", extra={"task_dir": task_dir})

    minio_service.put_file(signed_document, body["destination"])
    logger.info(
        f"Document: {signed_document} was uploaded to minio",
    )

    generate_preview_service.get_document_key(body["document"]).unlink(True)

    return {"url": minio_service.get_signed_url(body["destination"])}
