from dependency_injector.wiring import inject, Provide
from flask import blueprints
from logging import Logger
from pathlib import Path
from sign_api.decorators.authorized_decorator import authorized
from sign_api.decorators.validate_decorator import validate
from sign_api.services.generate_preview_service import GeneratePreviewService
from sign_api.services.minio_service import MinioService
from sign_api.settings import Settings
import uuid

from sign_api.utils import get_task_dir


generate_preview_controller = blueprints.Blueprint("generate_preview", __name__)


@generate_preview_controller.post("/generate_preview")
@authorized
@validate(
    {
        "document": {"type": "string", "required": True},
        "prefix": {
            "type": "string",
            "required": True,
            "check_with": lambda field, value, error: value.endswith("/")
            or error(field, "Prefix must end with /"),
        },
        "page": {"type": "integer", "required": False},
    }
)
@inject
def generate_preview(
    settings: Settings = Provide["settings"],
    logger: Logger = Provide["logger"],
    minio_service: MinioService = Provide["minio_service"],
    generate_preview_service: GeneratePreviewService = Provide[
        "generate_preview_service"
    ],
    body: dict = Provide["body"],
):
    document_key = generate_preview_service.get_document_key(body["document"])

    task_dir = Path(get_task_dir(settings.WORKDIR, uuid.uuid4().hex))
    logger.info(
        f"Start generating preview in task dir: {task_dir}",
        extra={"task_dir": task_dir},
    )

    if not document_key.exists():
        minio_service.get_file(body["document"], document_key)

    if body.get("page") is not None:
        images = generate_preview_service.generate_single_page(
            document_key, task_dir, body["page"]
        )
    else:
        images = generate_preview_service.generate_whole_document(
            document_key, task_dir
        )

    result = map(
        lambda image: minio_service.put_file(image, body["prefix"] + image.name), images
    )

    return {"urls": [minio_service.get_signed_url(image) for image in result]}
