import sys
from dotenv import load_dotenv
from flask import Flask
from sign_api.container import Container
from sign_api.controllers.sign_controller import sign_controller
from sign_api.controllers.generate_preview_controller import generate_preview_controller
from sign_api.controllers.count_pages_controller import count_pages_controller
from sign_api.controllers.enhance_signature_controller import (
    enhance_signature_controller,
)
from sign_api.controllers.convert_controller import convert_controller
from sign_api.handlers import handle_exception, parse_body
from sign_api.scheduler import create_scheduler

if "pytest" not in sys.modules:
    load_dotenv()  # pragma: no cover


def create_app():
    app = Flask(__name__)
    container = Container()
    app.config["container"] = container

    app.logger = container.logger()
    app.config.from_object(container.settings())

    app.before_request(parse_body)
    app.errorhandler(Exception)(handle_exception())

    app.register_blueprint(sign_controller)
    app.register_blueprint(generate_preview_controller)
    app.register_blueprint(count_pages_controller)
    app.register_blueprint(enhance_signature_controller)
    app.register_blueprint(convert_controller)

    @app.get("/")
    def hello():
        return "Hello World!"

    create_scheduler()

    return app
